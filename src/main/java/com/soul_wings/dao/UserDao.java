package com.soul_wings.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.soul_wings.modals.User;

public class UserDao extends GenericDaoImpl<User> {

	@SuppressWarnings("unchecked")
	public List<User> login(String password, String userName) {
		Criteria criteria = getSession().createCriteria(User.class);
		criteria.add(Restrictions.eq("password", password));
		criteria.add(Restrictions.eq("userName", userName));
		criteria.add(Restrictions.eq("isflag", 1));
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	public List<User> validate(String userName) {
		Criteria criteria = getSession().createCriteria(User.class);
		criteria.add(Restrictions.eq("userName", userName));
		criteria.add(Restrictions.eq("isflag", 1));
		return criteria.list();
	}

}
