package com.soul_wings.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import com.soul_wings.hibernate.HibernateUtil;

@SuppressWarnings("unchecked")
public class GenericDaoImpl<E> implements GenericDao<E> {
	private final Class<E> entityClass;

	public Session session;

	public GenericDaoImpl() {
		this.entityClass = (Class<E>) ((ParameterizedType) this.getClass().getGenericSuperclass())
				.getActualTypeArguments()[0];
	}

	public Session getSession() {
		session = HibernateUtil.getSessionFactory().getCurrentSession();

		if (session.getTransaction().isActive()) {
			return session;
		}
		session.beginTransaction();
		return session;

	}

	@Override
	public Serializable save(E entity) {
		session = getSession();
		Serializable s = session.save(entity);
		return s;
	}

	@Override
	public void saveOrUpdate(E entity) {
		session = getSession();
		session.saveOrUpdate(entity);
	}

	@Override
	public boolean deleteById(Class<?> type, Serializable id) {
		session = getSession();
		Object persistentInstance = session.load(type, id);
		if (persistentInstance != null) {
			session.delete(persistentInstance);
			return true;
		}

		return false;
	}

	@Override
	public void deleteAll() {
		session = getSession();
		List<E> entities = findAll();
		for (E entity : entities) {
			session.delete(entity);
		}
	}

	@Override
	@SuppressWarnings("deprecation")
	public List<E> findAll() {
		session = getSession();
		return session.createCriteria(this.entityClass).list();
	}

	@Override
	@SuppressWarnings("deprecation")
	public List<E> findAllByExample(E entity) {
		Example example = Example.create(entity).ignoreCase().enableLike().excludeZeroes();
		return session.createCriteria(this.entityClass).add(example).list();
	}

	@Override
	public E findById(Serializable id) {
		session = getSession();
		return session.get(this.entityClass, id);
	}

	@SuppressWarnings("deprecation")
	@Override
	public List<E> findAllWithLike(List<String> keys, String searchValue) {
		Disjunction disjunction = Restrictions.disjunction();
		Criterion criterion;
		for (int i = 0; i < keys.size(); i++) {
			criterion = Restrictions.ilike(keys.get(i), searchValue, MatchMode.START);
			disjunction.add(criterion);
		}
		Criteria crit = getSession().createCriteria(this.entityClass);
		crit.add(disjunction);
		List<E> results = crit.list();
		return results;
	}

	@Override
	public void clear() {
		session.clear();

	}

	@Override
	public void flush() {
		session.flush();
	}

	@Override
	public void close() {
		if (session.getTransaction().isActive()) {
			session.getTransaction().commit();
		}
	}

	@Override
	public void rollback() {
		if (session.getTransaction().isActive()) {
			session.getTransaction().rollback();
		}
	}
}
