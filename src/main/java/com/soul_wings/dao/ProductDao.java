package com.soul_wings.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.soul_wings.modals.Products;

public class ProductDao extends GenericDaoImpl<Products> {
	
	
	@SuppressWarnings("unchecked")
	public List<Products> validate(String barcodeId) {
		Criteria criteria = getSession().createCriteria(Products.class);
		criteria.add(Restrictions.eq("barcodeId", barcodeId));
		criteria.add(Restrictions.eq("isflag", 1));
		return criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Products> validate_sell(Float sellPrice) {
		Criteria criteria = getSession().createCriteria(Products.class);
		criteria.add(Restrictions.eq("sellPrice", sellPrice));
		criteria.add(Restrictions.eq("isflag", 1));
		return criteria.list();
	}
	
	

	@SuppressWarnings("unchecked")
	public List<Object> calc_sale(Date from, Date to) {
		
		Criteria criteria = getSession().createCriteria(Products.class);
		
		criteria.add(Restrictions.ge("sellDate", from));
		criteria.add(Restrictions.le("sellDate", to));
		criteria.setProjection(Projections.sum("sellPrice"));
		//Float sum =(Float)criteria.uniqueResult();
		
		
		return criteria.list();
	}
}
