package com.soul_wings.services.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.soul_wings.dao.ProductDao;
import com.soul_wings.dto.ExcelDto;
import com.soul_wings.dto.ProductDto;
import com.soul_wings.dto.ResponseModel;
import com.soul_wings.dto.SumDto;
import com.soul_wings.extensions.ConstantExtension;
import com.soul_wings.extensions.HelperExtension;
import com.soul_wings.extensions.ModelSetterExtensions;
import com.soul_wings.modals.Products;
import com.soul_wings.modals.User;
import com.soul_wings.services.def.ProductDef;

public class ProductImpl implements ProductDef {

	ProductDao dao = null;
	ObjectMapper objectmapper = null;

	@Override
	public ResponseModel saveOrUpdate(ProductDto dto) {
		ResponseModel responsemodel = new ResponseModel();

		dao = new ProductDao();
		objectmapper = new ObjectMapper();
		JsonNode jsonNode = null;
		ModelSetterExtensions modelsetterextension = new ModelSetterExtensions();

		try {
			List<ProductDto> dtos = new ArrayList<>();
			Products products = modelsetterextension.getProduct(dto);
			List<Products> check = dao.validate(dto.getBarcodeId());
			if (check.size() == 0) {

				dao.saveOrUpdate(products);
				dto.setBarcodeId(products.getBarcodeId());

				dtos.add(dto);
				jsonNode = objectmapper.valueToTree(dtos);
				responsemodel = new ResponseModel(true, ConstantExtension.SUCCESS_ADDED, jsonNode);
			} else {
				jsonNode = objectmapper.valueToTree(dtos);
				responsemodel = new ResponseModel(false, ConstantExtension.SUCCESS_MESSAGE_INSERT_ERROR, jsonNode);
			}
		} catch (Exception e) {
			dao.rollback();
			e.printStackTrace();
			responsemodel = new ResponseModel(false, ConstantExtension.MESSAGE_ERROR, jsonNode);
		} finally {
			dao.close();
		}
		return responsemodel;
	}

	@Override
	public ResponseModel saveOrUpdate_sell(String barcodeId, Float sellPrice, String comment, String userName) {
		ResponseModel responsemodel = new ResponseModel();

		dao = new ProductDao();
		objectmapper = new ObjectMapper();
		JsonNode jsonNode = null;

		try {
			// List<ProductDto> dtos = new ArrayList<>();
			Products model = dao.findById(barcodeId);
			List<Products> check = dao.validate_sell(model.getSellPrice());

			if (check.size() == 0) {
				model.setSellPrice(sellPrice);
				model.setSellDate(new Date());
				model.setComment(comment);

				User userModel = new User();
				userModel.setUserName(userName);

				model.setuserName(userModel);
				dao.saveOrUpdate(model);

				// jsonNode = objectmapper.valueToTree(dtos);
				responsemodel = new ResponseModel(true, ConstantExtension.SUCCESS_ADDED, jsonNode);
			} else {
				// jsonNode = objectmapper.valueToTree(dtos);
				responsemodel = new ResponseModel(false, ConstantExtension.SUCCESS_MESSAGE_INSERT_ERROR_SELL, jsonNode);
			}
		} catch (Exception e) {
			dao.rollback();
			e.printStackTrace();
			responsemodel = new ResponseModel(false, ConstantExtension.MESSAGE_ERROR, jsonNode);
		} finally {
			dao.close();
		}

		return responsemodel;
	}

	@Override
	public ResponseModel saveOrUpdate_return(String barcodeId, String comment) {
		ResponseModel responsemodel = new ResponseModel();

		dao = new ProductDao();
		objectmapper = new ObjectMapper();
		JsonNode jsonNode = null;
		// Products check = dao.findById(barcodeId);

		try {

			Products model = dao.findById(barcodeId);
			List<Products> return_check = dao.validate_sell(model.getSellPrice());
			if (return_check.size() > 0) {

				model.setReturnDate(new Date());
				model.setComment(comment);
				model.setSellPrice(null);
				model.setSellDate(null);

				dao.saveOrUpdate(model);

				responsemodel = new ResponseModel(true, ConstantExtension.SUCCESS_ADDED, jsonNode);
			} else {
				responsemodel = new ResponseModel(false, ConstantExtension.SUCCESS_MESSAGE_RETURN_ERROR, jsonNode);
			}
		} catch (Exception e) {
			dao.rollback();
			e.printStackTrace();
			responsemodel = new ResponseModel(false, ConstantExtension.MESSAGE_ERROR, jsonNode);
		} finally {
			dao.close();
		}

		return responsemodel;
	}

	@Override
	public ResponseModel downloadExcel() {
		ResponseModel responsemodel = new ResponseModel();
		HelperExtension helperExtension = new HelperExtension();
		dao = new ProductDao();
		objectmapper = new ObjectMapper();
		JsonNode jsonNode = null;
		try {
			List<Products> model = dao.findAll();
			// create excel xls sheet
			Workbook workbook = new XSSFWorkbook();
			Sheet sheet = workbook.createSheet("Products Detail");
			sheet.setDefaultColumnWidth(30);

			// create style for header cells
			CellStyle style = workbook.createCellStyle();
			Font font = workbook.createFont();
			font.setFontName("Arial");
			style.setFillForegroundColor(HSSFColor.BLUE.index);
			// style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			// font.setBold(true);
			font.setColor(HSSFColor.BLACK.index);
			style.setFont(font);

			// create header row
			Row header = sheet.createRow(0);
			header.createCell(0).setCellValue("BarcodeId");
			header.getCell(0).setCellStyle(style);
			header.createCell(1).setCellValue("Product_Type");
			header.getCell(1).setCellStyle(style);
			header.createCell(2).setCellValue("Product_Size");
			header.getCell(2).setCellStyle(style);
			header.createCell(3).setCellValue("Purchase_Price");
			header.getCell(3).setCellStyle(style);
			header.createCell(4).setCellValue("Sell_Price");
			header.getCell(4).setCellStyle(style);
			header.createCell(5).setCellValue("Purchase_Date");
			header.getCell(5).setCellStyle(style);
			header.createCell(6).setCellValue("Sell_Date");
			header.getCell(6).setCellStyle(style);
			header.createCell(7).setCellValue("Comment");
			header.getCell(7).setCellStyle(style);
			header.createCell(8).setCellValue("Return_Date");
			header.getCell(8).setCellStyle(style);
			header.createCell(9).setCellValue("User_Name");
			header.getCell(9).setCellStyle(style);

			int rowCount = 1;
			for (Products product : model) {
				Row userRow = sheet.createRow(rowCount++);
				userRow.createCell(0).setCellValue(product.getBarcodeId());
				userRow.createCell(1).setCellValue(product.getProductType());
				userRow.createCell(2).setCellValue(product.getProductSize());
				userRow.createCell(3).setCellValue(product.getPuchasePrice());

				if (!helperExtension.isNullOrEmpty(product.getSellPrice())) {
					userRow.createCell(4).setCellValue(product.getSellPrice());
				}

				userRow.createCell(5).setCellValue(product.getPurchaseDate());

				if (!helperExtension.isNullOrEmpty(product.getSellDate())) {
					userRow.createCell(6).setCellValue(product.getSellDate());
				}
				if (!helperExtension.isNullOrEmpty(product.getComment())) {
					userRow.createCell(7).setCellValue(product.getComment());
				}
				if (!helperExtension.isNullOrEmpty(product.getReturnDate())) {
					userRow.createCell(8).setCellValue(product.getReturnDate());
				}

				if (!helperExtension.isNullOrEmpty(product.getuserName())) {
					if (!helperExtension.isNullOrEmpty(product.getuserName().getUserName())) {
						userRow.createCell(9).setCellValue(product.getuserName().getUserName());
					}
				}
			}

			FileOutputStream out = new FileOutputStream(
					new File(ConstantExtension.base_path + ConstantExtension.file_path));
			workbook.write(out);
			out.close();

			ExcelDto excelDto = new ExcelDto();
			excelDto.setLink("excel.xlsx");
			jsonNode = objectmapper.valueToTree(excelDto);

			responsemodel = new ResponseModel(true, ConstantExtension.SUCCESS_DOWNLOADED, jsonNode);
		} catch (Exception e) {
			dao.rollback();
			e.printStackTrace();
			responsemodel = new ResponseModel(false, ConstantExtension.MESSAGE_ERROR, jsonNode);
		} finally {
			dao.close();
		}

		return responsemodel;
	}

	@Override
	public ResponseModel calculateSale(long from1, long to1) {
		HelperExtension helperExtension = new HelperExtension();
		Date from = helperExtension.timestampToDate(from1), to = helperExtension.timestampToDate(to1);
		ResponseModel responsemodel = new ResponseModel();
		dao = new ProductDao();
		objectmapper = new ObjectMapper();
		JsonNode jsonNode = null;

		try {

			List<Object> listObj = dao.calc_sale(from, to);
			SumDto dto = new SumDto();
			dto.setSum(listObj.get(0) + "");
			jsonNode = objectmapper.valueToTree(dto);

			responsemodel = new ResponseModel(true, ConstantExtension.SUCCESS_RECEIVE, jsonNode);

		} catch (Exception e) {
			dao.rollback();
			e.printStackTrace();
			responsemodel = new ResponseModel(false, ConstantExtension.MESSAGE_ERROR, jsonNode);
		} finally {
			dao.close();
		}

		return responsemodel;
	}

}