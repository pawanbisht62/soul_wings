package com.soul_wings.services.impl;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.soul_wings.dao.UserDao;
import com.soul_wings.dto.ResponseModel;
import com.soul_wings.dto.UserDto;
import com.soul_wings.extensions.ConstantExtension;
import com.soul_wings.extensions.DTOSetterExtension;
import com.soul_wings.extensions.ModelSetterExtensions;
import com.soul_wings.modals.User;
import com.soul_wings.services.def.UserDef;

public class UserImpl implements UserDef {

	UserDao dao = null;
	ObjectMapper objectmapper = null;

	@Override
	public ResponseModel login(String userName, String password) {
		ResponseModel responseModel = new ResponseModel();
		JsonNode jsonNode = null;
		objectmapper = new ObjectMapper();
		dao = new UserDao();
		try {
			List<UserDto> dtos = new ArrayList<>();
			List<User> user = dao.login(password,userName);

			if (user.size() > 0) {
				for (User model : user) {
					UserDto dto1 = new DTOSetterExtension().getUserDto(model);
					dtos.add(dto1);
				}

				jsonNode = objectmapper.valueToTree(dtos);
				responseModel = new ResponseModel(true, ConstantExtension.SUCCESS_MESSAGE_USER_TRUE, jsonNode);
			} else {
				jsonNode = objectmapper.valueToTree(dtos);
				responseModel = new ResponseModel(false, ConstantExtension.SUCCESS_MESSAGE_USER_FALSE, jsonNode);
			}
		} catch (Exception e) {
			dao.rollback();
			e.printStackTrace();
			responseModel = new ResponseModel(false, ConstantExtension.MESSAGE_ERROR, jsonNode);
		} finally {
			dao.close();
		}
		return responseModel;
	}

	@Override
	public ResponseModel saveOrUpdate(UserDto dto) {
		ResponseModel responsemodel = new ResponseModel();
		dao = new UserDao();
		objectmapper = new ObjectMapper();
		JsonNode jsonnode = null;
		ModelSetterExtensions modelsetterextension = new ModelSetterExtensions();

		try {

			List<UserDto> dtos = new ArrayList<>();
			User user = modelsetterextension.getUser(dto);
			List<User> check = dao.validate(dto.getUserName());
			if (check.size() == 0) {
				dao.saveOrUpdate(user);
				dto.setUserName(user.getUserName());
				
				dtos.add(dto);
				jsonnode = objectmapper.valueToTree(dtos);
				responsemodel = new ResponseModel(true, ConstantExtension.SUCCESS_ADDED, jsonnode);
			} else {
				jsonnode = objectmapper.valueToTree(dtos);
				responsemodel = new ResponseModel(false, ConstantExtension.SUCCESS_MESSAGE_SIGNUP_ERROR, jsonnode);
			}
		} catch (Exception e) {
			dao.rollback();
			e.printStackTrace();
			responsemodel = new ResponseModel(false, ConstantExtension.MESSAGE_ERROR, jsonnode);
		} finally {
			dao.close();
		}
		return responsemodel;
	}




}