package com.soul_wings.services.def;


import com.soul_wings.dto.ProductDto;
import com.soul_wings.dto.ResponseModel;

public interface ProductDef {

	public ResponseModel saveOrUpdate(ProductDto dto);
	
	public ResponseModel saveOrUpdate_sell(String barcodeId, Float sellPrice, String comment, String userName);
	
	public ResponseModel saveOrUpdate_return(String barcodeId, String Comment );
	
	public ResponseModel downloadExcel();

	ResponseModel calculateSale(long from1, long to1);
	

}
