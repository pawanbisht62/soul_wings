package com.soul_wings.services.def;

import com.soul_wings.dto.ResponseModel;
import com.soul_wings.dto.UserDto;

public interface UserDef {

	public ResponseModel login(String userName, String password);
	
	public ResponseModel saveOrUpdate(UserDto dto);
		
	

}
