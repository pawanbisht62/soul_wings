package com.soul_wings.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.soul_wings.dto.ResponseModel;
import com.soul_wings.dto.UserDto;
import com.soul_wings.extensions.ResponseCreatorExtension;
import com.soul_wings.services.impl.UserImpl;

@Path("/user")
public class UserController {

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response login(@HeaderParam("userName") String userName, @HeaderParam("password") String password) throws JsonProcessingException {
		ResponseModel responseModel = new UserImpl().login(userName,password);

		return ResponseCreatorExtension.responseCreator(responseModel);
	}
	
	@Path("/user_create")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response SaveOrUpdate(UserDto dto) throws JsonProcessingException {
		ResponseModel responsemodel = new UserImpl().saveOrUpdate(dto);
		return ResponseCreatorExtension.responseCreator(responsemodel);
	}
	
}
