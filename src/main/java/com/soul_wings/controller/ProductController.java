package com.soul_wings.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.soul_wings.dto.ProductDto;
import com.soul_wings.dto.ResponseModel;
import com.soul_wings.extensions.ResponseCreatorExtension;
import com.soul_wings.services.impl.ProductImpl;


@Path("/product_buy")
public class ProductController {

	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)

	public Response SaveOrUpdate(ProductDto dto) throws JsonProcessingException {
		ResponseModel responsemodel = new ProductImpl().saveOrUpdate(dto);
		return ResponseCreatorExtension.responseCreator(responsemodel);
	}
	
	@Path("/product_sell")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)

	public Response SaveOrUpdate_Sell(@HeaderParam("barcodeId") String barcodeId,@HeaderParam("sellPrice") Float sellPrice,  @HeaderParam("comment") String comment, @HeaderParam("userName") String userName) throws JsonProcessingException {
		ResponseModel responsemodel = new ProductImpl().saveOrUpdate_sell(barcodeId, sellPrice, comment, userName);
		return ResponseCreatorExtension.responseCreator(responsemodel);
	}
	
	@Path("/product_return")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)

	public Response SaveOrUpdate_Return(@HeaderParam("barcodeId") String barcodeId, @HeaderParam("comment") String comment) throws JsonProcessingException {
		ResponseModel responsemodel = new ProductImpl().saveOrUpdate_return(barcodeId, comment);
		return ResponseCreatorExtension.responseCreator(responsemodel);
	}
	
	@Path("/download_excel")
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)

	public Response DownloadExcel() throws JsonProcessingException {
		ResponseModel responsemodel = new ProductImpl().downloadExcel();
		return ResponseCreatorExtension.responseCreator(responsemodel);
	}
	
	@Path("/sale_calculate")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response CalculateSale(@HeaderParam("dateFrom") long from, @HeaderParam("dateTo") long to) throws JsonProcessingException {
		ResponseModel responsemodel = new ProductImpl().calculateSale(from, to);
		return ResponseCreatorExtension.responseCreator(responsemodel);
	}
	
	
}
