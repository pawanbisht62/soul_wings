
package com.soul_wings.extensions;


import com.soul_wings.dto.ProductDto;
import com.soul_wings.dto.UserDto;
import com.soul_wings.modals.Products;
import com.soul_wings.modals.User;

public class DTOSetterExtension {
	HelperExtension helperExtension = new HelperExtension();

	public UserDto getUserDto(User model) {
		UserDto dto = null;

		if (!helperExtension.isNullOrEmpty(model)) {
			dto = new UserDto();

			if (!helperExtension.isNullOrEmpty(model.getUserName()))
				dto.setUserName(model.getUserName());

			dto.setUserName(model.getUserName());
			dto.setPassword(model.getPassword());

		}
		return dto;
	}

	public ProductDto getProductDto(Products model) {
		ProductDto dto = null;

		if (!helperExtension.isNullOrEmpty(model)) {
			dto = new ProductDto();

			if (!helperExtension.isNullOrEmpty(model.getBarcodeId()))
				dto.setBarcodeId(model.getBarcodeId());

			dto.setProductType(model.getProductType());
			dto.setProductSize(model.getProductSize());
			dto.setPuchasePrice(model.getPuchasePrice());
			dto.setPurchaseDate(model.getPurchaseDate());
			dto.setSellDate(model.getSellDate());
			dto.setSellPrice(model.getSellPrice());
			dto.setReturnDate(model.getReturnDate());
			dto.setComment(model.getComment());
			
			if (!helperExtension.isNullOrEmpty(model.getuserName()))
				dto.setUserName(getUserDto(model.getuserName()));

		}
		return dto;
	}
			
}	
