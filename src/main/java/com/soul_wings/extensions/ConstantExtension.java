package com.soul_wings.extensions;

public class ConstantExtension {

	public static String MESSAGE_ERROR = "Internal Server Error";
	public static String SUCCESS_ADDED = "Successfully Added";
	public static String SUCCESS_RECEIVE = "Successfully Received";
	public static String SUCCESS_UPDATED = "Successfully Updated";
	public static String SUCCESS_DOWNLOADED = "Successfully Downloaded";
	public static String UPDATION_ERROR = "Not Updated";
	public static String FETCHING_ERROR = "Data not Received";
	public static String DELETION_ERROR = "Not Deleted yet";
	

	public static String DUPLICACY_ERROR = "Product is Already in the List!!";
	public static String SUCCESS_MESSAGE_INSERT_ERROR = "Barcode-Id already exist";
	public static String SUCCESS_MESSAGE_SIGNUP_ERROR = "User-Name already exist";
	public static String SUCCESS_MESSAGE_INSERT_ERROR_SELL = "Already Sold";
	public static String SUCCESS_MESSAGE_RETURN_ERROR = "Product Not sold";
	public static String SUCCESS_MESSAGE_LOGIN_FAILED = "Bad Credential";

	// user login
	public static String SUCCESS_MESSAGE_USER_TRUE = "Welcome user";
	public static String SUCCESS_MESSAGE_USER_FALSE = "Wrong credentials";

	// adminLogin
	public static String SUCCESS_MESSAGE_ADMIN_TRUE = "Welcome admin";
	public static String SUCCESS_MESSAGE_ADMIN_FALSE = "Wrong credentials";
	public static String ADMIN_WRONG_PASSWORD = "Old Password is not Correct";
	public static String ADMIN_PASSWORD_CHANGED = "Password Changed Successfully";

	public static String SUCCESS_MESSAGE = "Successfully Fetched";
	public static String SUCCESS_MESSAGE_UPDATED = "Successfully Updated";
	public static String SUCCESS_MESSAGE_DELETED = "Successfully Deleted";
	public static String MSG_PHONE_NUMBER_MISSING = "Phone Number is missing";
	public static String MESSAGE_LOGOUT = "Logout Successfully";
	
	
	//Excel related
	public static String file_path = "/soul_wings/excel.xlsx";
	public static String base_path = "C:\\Program Files\\Apache Software Foundation\\Tomcat 9.0\\webapps";
	
	
	// Paths
	/*
	 * public static String FolderPath = ECommerceExtension.getFolderPath(); public
	 * static String DomainName = ECommerceExtension.getDomainPath();
	 * 
	 * public static String FolderPathPostkards = FolderPath +
	 * "Images\\Postkards\\"; public static String FolderPathSignatures = FolderPath
	 * + "Images\\Signatures\\"; public static String FolderPathFonts = FolderPath +
	 * "assets\\tool\\fonts\\"; public static String FolderPathThemes = FolderPath +
	 * "assets\\images\\themes";
	 * 
	 * public static String DatabasePathPostkards = "Images/Postkards/"; public
	 * static String DatabasePathSignatures = "Images/Signatures/"; public static
	 * String DatabasePathFonts = "assets/fonts/";
	 */
}
