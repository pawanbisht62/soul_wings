package com.soul_wings.extensions;



import java.util.Date;

import com.soul_wings.dto.ProductDto;
import com.soul_wings.dto.UserDto;
import com.soul_wings.modals.Products;
import com.soul_wings.modals.User;

public class ModelSetterExtensions {
	HelperExtension helperExtension = new HelperExtension();
	
	
	public Products getProduct(ProductDto dto) {
		Products model = null;

		if (!helperExtension.isNullOrEmpty(dto)) {
			model = new Products();

			if (!helperExtension.isNullOrEmpty(dto.getBarcodeId()))
				model.setBarcodeId(dto.getBarcodeId());

			model.setProductType(dto.getProductType());
			model.setProductSize(dto.getProductSize());
			model.setPuchasePrice(dto.getPuchasePrice());
			model.setPurchaseDate(new Date());
			model.setSellPrice(dto.getSellPrice());
			//model.setSellDate(new Date());
			model.setComment(dto.getComment());
			//model.setReturnDate(dto.getReturnDate());
			model.setDateTime(helperExtension.getDateTime());
			model.setIsflag(1);
			
			/*if (!helperExtension.isNullOrEmpty(dto.getUserName()))
				model.setuserName(getUser(dto.getUserName()));
*/
			
			
			
		}
		return model;
	}
	public User getUser(UserDto dto) {
		User model = null;

		if (!helperExtension.isNullOrEmpty(dto)) {
			model = new User();

			if (!helperExtension.isNullOrEmpty(dto.getUserName()))
				model.setUserName(dto.getUserName());
			
			model.setPassword(dto.getPassword());
			model.setDateTime(new Date());
			model.setIsflag(1);
	
		}
		return model;
	}	
	
}
	
